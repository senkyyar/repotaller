var customersObtenidos;

function getCustomers() {
  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  //var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers?$filter=Country eq 'Germany'";
  var request = new XMLHttpRequest();
  request.onreadystatechange = function () {
    if( this.readyState == 4 && this.status == 200 ) {
      console.log( request.responseText );
      customersObtenidos = request.responseText;
      procesarCustomers();
    }
  }
  request.open( "GET", url, true );
  request.send();
}

function procesarCustomers() {
  var JSONCustomers = JSON.parse( customersObtenidos );
  var tabla = document.getElementById( "customers" );
  var filas = "";
  for (var i = 0; i < JSONCustomers.value.length; i++) {
    customer = JSONCustomers.value[i];
    //filas += "<tr><th scope='row'>" + customer.CustomerID + "</th><td>" + customer.ContactName + "</td><td>" + customer.City + "</td><td>" + customer.Phone + "</td><td>" + customer.Country + "</td></tr>";
    if( customer.Country == 'UK' )
      img = "<img class='flag' src='https://www.countries-ofthe-world.com/flags-normal/flag-of-United-Kingdom.png'/>"
    else
      img = "<img class='flag' src='https://www.countries-ofthe-world.com/flags-normal/flag-of-" + customer.Country + ".png'/>"
    filas += "<tr><th scope='row'>" + customer.CustomerID + "</th><td>" + customer.ContactName + "</td><td>" + customer.City + "</td><td>" + customer.Phone + "</td><td>" + img + "</td></tr>";
  }
  tabla.innerHTML = filas;
}

(getCustomers());
