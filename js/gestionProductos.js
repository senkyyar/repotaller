var productosObtenidos;

function getProductos() {
  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Products";
  var request = new XMLHttpRequest();
  request.onreadystatechange = function () {
    if( this.readyState == 4 && this.status == 200 ) {
      console.log( request.responseText );
      productosObtenidos = request.responseText
      procesarProductos();
    }
  }
  request.open( "GET", url, true );
  request.send();
}

function procesarProductos() {
  var JSONProductos = JSON.parse( productosObtenidos );
  //alert( JSONProductos.value[0].ProductName );
  var tabla = document.getElementById( "productos" );
  var filas = "";
  for (var i = 0; i < JSONProductos.value.length; i++) {
    producto = JSONProductos.value[i];
    filas += "<tr><th scope='row'>" + producto.ProductID + "</th><td>" + producto.ProductName + "</td><td>" + producto.UnitPrice + "</td><td>" + producto.UnitsInStock + "</td></tr>";
  }
  tabla.innerHTML = filas;
}

(getProductos());
